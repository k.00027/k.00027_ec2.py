import json
import random
from statistics import mean, stdev

def simulate(data):
    close = data['close']
    buy = data['buy']
    sell = data['sell']
    h = int(data['h'])
    d = int(data['d'])
    t = data['t']

    var95_list = []
    var99_list = []
    dates = []

    for i in range(h, len(close)):
        if (t == 'buy' and buy[i] == 1) or (t == 'sell' and sell[i] == 1):
            close_data = close[i-h:i]
            pct_change = [(close_data[j] - close_data[j-1]) / close_data[j-1] for j in range(1, len(close_data))]
            mn = mean(pct_change)
            sd = stdev(pct_change)
            simulations = [random.gauss(mn, sd) for _ in range(d)]
            simulations.sort(reverse=True)  # Sorting in descending order
            var95 = simulations[int(0.95 * len(simulations)) - 1]  # Adjusted for 0-based index
            var99 = simulations[int(0.99 * len(simulations)) - 1]  # Adjusted for 0-based index

            dates.append(data['dates'][i])
            var95_list.append(var95)
            var99_list.append(var99)

    response_body = {
        'dates': dates,
        'var95': var95_list,
        'var99': var99_list
    }

    return response_body